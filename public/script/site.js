//Copy To Clipboard
function copyToClipboard(selector) {
    var $temp = $("<div>");
    $("body").append($temp);
    $temp.attr("contenteditable", true)
        .html($(selector).html()).select()
        .on("focus", function () {
            document.execCommand('selectAll', false, null);
        })
        .focus();
    document.execCommand("copy");
    $temp.remove();
}
$(document).scroll(function () {
    var y = $(document).scrollTop(), //get page y value
        header = $(".criteria"); // your div id
    if (y >= 200) {
        header.css({
            position: "fixed",
            "top": "0",
            "left": "0",
            "width": "100%",
            "background-color": "orange"
        });
    } else {
        header.css({
            "position": "static",
            "background-color": "transparent"
        });
    }
});
$(document).ready(function () {
    //============================================

    //ALL PRODUCTS PAGE
    //Show Card
    $("a.kart").on("click", () => {
        if (!$(this).hasClass("active")) {
            $("a.liste").removeClass("active");
            $("#table").removeClass("d-block").addClass("d-none");
            $(this).addClass("active");
            $(".side").removeClass("d-none").addClass("d-block");
            $(".container#cards").removeClass("d-none").addClass("d-flex");
        }
    });
    //Show List
    $("a.liste").on("click", () => {
        if (!$(this).hasClass("active")) {
            $("a.kart").removeClass("active");
            $(".container#cards").removeClass("d-flex").addClass("d-none");
            $(this).addClass("active");
            $(".side").removeClass("d-block").addClass("d-none");
            $("#table").removeClass("d-none").addClass("d-block");
        }
    });
    //Side Menu Toggle
    $(".side .toggle-btn a").on("click", function () {
        $(".side #sidemenu").toggle("slow");
    });
    //Side Menu Toggle
    //ALL PRODUCTS PAGE
    //============================================
    //Toggle Columns
    for (var i = 1; i <= $("table tr th").length; i++) {
        $(`table thead tr :nth-child(${i})`).addClass(i.toString());
    }
    for (var k = 1; k <= $("table tr th").length; k++) {
        $(`table tbody tr :nth-child(${k})`).addClass(k.toString());
    }
    $("#display a.display-btn").on("click", function () {
        var col = $(this).attr("id");
        $("#display a#" + col.toString()).addClass("hide");
        $("table ." + col.toString()).toggle();
        console.log(col);
    });
    //=========================================================

    //=========================================================
    //Search In Cards
    function searchTable(val) {
        for (var t = 0; t < $(".card-body").length; t++) {
            if ($(".card-body").eq(t).children().eq(1).text().trim().toLowerCase().indexOf(val.toLowerCase()) === -1) {
                $(".card-body").eq(t).parent().parent().removeClass("d-flex").addClass("d-none");
            } else {
                $(".card-body").eq(t).parent().parent().removeClass("d-none").addClass("d-flex");
            }
        }
    }
    $("#searchTable").on("keyup", function () {
        var val = $(this).val();
        searchTable(val);
    });
    //================================================
    //Side Menu Filter Types
    function searchType(arr) {
        /* Grid */
        for (var j = 0; j < $(".card").length; j++) {
            console.log($(".card p#type").eq(j).children().eq(1).text().trim().toLowerCase());
            console.log(arr.toString().toLowerCase().indexOf($(".card p#type").eq(j).children().eq(1).text().trim().toLowerCase()));
            if (arr.toString().indexOf($(".card p#type").eq(j).children().eq(1).text().trim().toLowerCase()) === -1) {
                $(".card").eq(j).addClass("d-none");
            } else {
                $(".card").eq(j).removeClass("d-none");
            }
        }
        /* Grid */
    }
    $("input[name='tur']").on("change", function () {
        var selected = [];
        $.each($("input[name='tur']"), function () {
            if ($(this).prop("checked")) {
                selected.push($(this).val());
            }
        });
        searchType(selected);
    });
    //=========================================================
    //=========================================================
    //Pager
    var numPages = Math.ceil(($('.card').length) / 5);
    if (numPages > 1) {
        for (i = 2; i <= numPages; i++) {
            var element = " <li class='page-item'><a class = 'page-link' href='#'>" + i + "</a></li>";
            $("li.page-item").insertAfter(element);
        }
        //Add index to title and text
        var que = 0;
        $('.card').each(function (i, obj) {
            $(this).parent().attr("data-index", que);
            que++;
        });
        changePage();
    } else {
        console.log("Ürün Yok");
    }
    //change pagefunction
    function changePage() {
        var activePage = parseInt($(".pagination li.active").text());
        var firstElem = (activePage - 1) * 5;
        $('.cards').css("display", "none");
        $(".cards").css("display", "none");
        for (i = firstElem; i <= firstElem + 4; i++) {
            $("div[data-index=" + i + "]").css("display", "block");
        }
        $(".result-no p").text("Toplam " + parseInt($('.cards').length) + " Sonuçtan " + parseInt(firstElem + 1) + " ila " + parseInt($("#results div:visible:last").data("index") + 1) + " arasındakiler gösteriliyor:");

    }
    $(".pagination a").on("click", function () {
        $(".pagination a").parent().removeClass("active");
        $(this).parent().addClass("active");
        changePage();
    });

    //Pager

    //Homepage
    var weatherKey = "wXrNdFqfLGCNfi36z6wVJXCKQstosSL8",
        newsKey = "6de22856e599459daab4bf99eef879cf",
        key = "318251";

    //=============================================
    //Vakitler
    async function salah() {
        try {
            var response = await fetch("https://ezanvakti.herokuapp.com/vakitler?ilce=9541");
            var vakitler = await response.json();
            await $("#tarih").append(vakitler[0].MiladiTarihUzun);
            await $("#imsak").append(vakitler[0].Imsak);
            await $("#sabah").append(vakitler[0].Gunes);
            await $("#oglen").append(vakitler[0].Ogle);
            await $("#ikindi").append(vakitler[0].Ikindi);
            await $("#aksam").append(vakitler[0].Aksam);
            await $("#yatsi").append(vakitler[0].Yatsi);
            await $("#ay").attr("src", vakitler[0].AyinSekliURL);

        } catch (error) {
            console.log("Problem in salah api" + error);
            throw error;
        }
    }

    //Vakitler
    //================================================
    //Hava Durumu
    async function weatherWithCode() {
        try {
            var response = await fetch(`https://dataservice.accuweather.com/currentconditions/v1/${key}?apikey=${weatherKey}&language=tr`);
            var weather = await response.json();
            await $("#sicaklik").text(weather[0].Temperature.Metric.Value);
            await $("#derece").text(weather[0].Temperature.Metric.Unit);
            await $("#havaDurumu").text(weather[0].WeatherText);
            await $("#havaLink").attr("href", weather[0].MobileLink)
            await (weather[0].WeatherIcon + "").length === 1 ? weather[0].WeatherIcon = "0" + weather[0].WeatherIcon : weather[0].WeatherIcon = weather[0].WeatherIcon;
            await $("#havaIkon").attr("src", `https://vortex.accuweather.com/adc2010/m/images/icons/600x212/slate/${weather[0].WeatherIcon}.png`);
        } catch (error) {
            console.log(error);
            throw error;
        }
    }
    if (location.pathname === "/") {
        salah();
        weatherWithCode();
    }

});