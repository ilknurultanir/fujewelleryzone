var express = require("express"),
    bodyParser = require("body-parser"),
    Jewel = require("./modals/jewel"),
    mongoose = require('mongoose'),
    methodOverride=require("method-override"),
    fetch = require("node-fetch");
    app = express();
    //mongoose.connect("mongodb://username:password/collectionName");

var indexRoutes = require('./routes/index'),
    productRoutes = require('./routes/product');

app.set("view engine", "ejs");
app.set('trust proxy', true);
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(__dirname + '/public'));
app.use("/", indexRoutes);
app.use(productRoutes);
app.use(methodOverride('_method'));

//Server
const port = 3000;
app.listen(process.env.PORT || 3000, () => {
    console.log("Server runs at 3000");
});

