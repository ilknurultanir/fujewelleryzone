var express = require("express"),
    Jewel = require("../modals/jewel"),
    path = require("path"),
    fs = require("fs"),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override"),
    router = express.Router();
router.use(methodOverride('_method'));
var file = function (location, query) {
    var arr = [];
    var base = "/images/";
    var loc = path.join(__dirname + "/../public/images/" + location);
    var files = fs.readdirSync(loc);
    files.forEach(file => {
        if (file.indexOf(query) > -1) {
            if (file.split(query)[1][0] === "." || file.split(query)[1][0] === "(") {
                arr.push(base + "" + location + "/" + file);
            }

        }
    });
    return (arr);
};

/*index page*/
router.get("/urunler", (req, res) => {
    Jewel.find({}, (err, jewels) => {
        if (err) {
            console.log(err);
            res.redirect("/");
        } else {
            console.log(jewels);
            jewels.forEach(jewel => {
                jewel.images = file(jewel.type.toLowerCase(), jewel.product_code.toUpperCase());
                jewel.type =  jewel.type.slice(0,1).toUpperCase() + jewel.type.slice(1); 
                jewel.product_code = jewel.product_code.toUpperCase();
            });
            console.log(jewels);
            res.render("urun/tum", {
                jewels: jewels
            });
        }
    });
});

// /*add new to db*/
// router.post("/", (req, res) => {
//     switch (req.body.product.type) {
//         case ("kolye"):
//             req.body.product.product_code = "ko" + req.body.product.id;
//             break;
//         case ("yüzük"):
//             req.body.product.product_code = "y" + req.body.product.id;
//             break;
//         case ("bileklik"):
//             req.body.product.product_code = "b" + req.body.product.id;
//             break;
//         case ("küpe"):
//             req.body.product.product_code = "k" + req.body.product.id;
//             break;
//         case ("set"):
//             req.body.product.product_code = "s" + req.body.product.id;
//             break;
//     }
//     Jewel.create(req.body.product, (err, newJewel) => {
//         if (err) {
//             console.log(err);
//         } else {
//             res.redirect("/urunler");
//         }

//     });
// });
// /*add new page*/
// router.get("/urun/yeni", (req, res) => {
//     res.render("urun/yeni");
// });
/*Show product*/
router.get("/urun/:id", (req, res) => {
    Jewel.findById(req.params.id, (err, foundProduct) => {
        if (err) {
            console.log(err);
            res.redirect("/urun/" + req.params.id);
        } else {
            foundProduct.images = file(foundProduct.type, foundProduct.product_code.toUpperCase());
            console.log(foundProduct.images);
            res.render("urun/goster", {
                jewel: foundProduct
            });
        }
    });

});

// /*Show Edit Page of the Product*/
// router.get("/urun/:id/duzenle", (req, res) => {
//     Jewel.findById(req.params.id, (err, foundProduct) => {
//         if (err) {
//             console.log(err);
//             res.redirect("/urun/" + req.params.id);
//         } else {
//             res.render("urun/duzenle", {
//                 jewel: foundProduct
//             });
//         }
//     });
// });

// /*Update Product*/
// router.put("/urun/:id", (req, res) => {
//     Jewel.findByIdAndUpdate(req.params.id, req.body.product, (err, changedProduct) => {
//         if (err) {
//             console.log(err);
//             res.redirect("/urun/" + req.params.id);
//         } else {
//             res.redirect("/urun/" + req.params.id);
//         }
//     });
// });

// /*Delete Product*/
// router.delete("/urun/:id", (req, res) => {
//     Jewel.findByIdAndRemove(req.params.id, (err) => {
//         if (err) {
//             console.log(err);
//             res.redirect("/urunler");
//         } else {
//             console.log("ürün silindi");
//             res.redirect("/urunler");
//         }
//     });
// });




module.exports = router;