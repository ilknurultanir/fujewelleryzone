var express = require("express"),
    Jewel = require("../modals/jewel"),
    fetch = require("node-fetch"),
    bodyParser = require("body-parser"),
    weatherKey = "wXrNdFqfLGCNfi36z6wVJXCKQstosSL8",
    newsKey = "6de22856e599459daab4bf99eef879cf",
    ipNo = "95.6.16.140";
router = express.Router();

/* Homepage */
router.get("/", (req, res) => {
    res.render("homepage");
});
/* Homepage */

/* Search page */
router.post("/search", (req, res) => {
    if (req.body.search.images == "false") {
        req.body.search.images = [""];
    } else if (req.body.search.images == "true") {
        req.body.search.images = {
            $ne: [""]
        };
    }
    if (req.body.search.id === "") {
        delete req.body.search.id;
    }
    console.log(req.body.search);
    Jewel.find(req.body.search, (err, foundData) => {
        if (err) {
            console.log(err);
            res.redirect("/");
        } else if (foundData.length < 1) {
            res.render("search", {
                search: req.body.search,
                msg: "Aramanızla eşleşen ürün bulunamadı."
            });
        } else {
            console.log(foundData);
            res.render("search", {
                search: req.body.search,
                found: foundData,
            });

        }
    });

});

router.get("/search", (req, res) => {
    res.redirect("/");
});
/* Search Page */
module.exports = router;