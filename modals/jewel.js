var mongoose = require("mongoose");

var jewelSchema = new mongoose.Schema({
    id:{type:String, default:"000"},
    name:{type:String, default:"isimsiz"},
    type:{type:String, default:"küpe"},
    product_code:{type:String, default:"k000"},
    typestamp:{ type: Date, default: Date.now },
    price:{type:String, default:"0"},
    cost:{type:String, default:"0"},
    size:{type:String, default:"Null"},
    instagram:{type:String, default:"false"},
    images:{type:Object},
    stock:{type:String, default:"1"},
});


module.exports = mongoose.model("Jewel",jewelSchema);
